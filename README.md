### What is this repository for? ###

* Quick summary

Testing tools for the Ennatuurlijk project

* Version

0.0.1

### How do I get set up? ###

Checkout repo with Git and import into your favourite IDE

* Configuration

Have Ennatuurlijk.ini in your tomcat user's home dir 

* Dependencies

SoapUI must be installed on your system!

* How to run tests

simply by invoking maven test/maven install

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Sietze DIjkstra