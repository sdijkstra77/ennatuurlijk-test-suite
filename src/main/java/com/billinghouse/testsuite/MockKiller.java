package com.billinghouse.testsuite;

import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


public class MockKiller
{

  public MockKiller()
  {
    // TODO Auto-generated constructor stub
  }

  public void run()
  {
    try {
      Process p1 = Runtime.getRuntime().exec(new String[] { "ps", "-ef" });
      InputStream input = p1.getInputStream();
      Process p2 = Runtime.getRuntime().exec(new String[] { "grep", "soap" });
      OutputStream output = p2.getOutputStream();
      IOUtils.copy(input, output);
      output.close(); // signals grep to finish
      List<String> result = IOUtils.readLines(p2.getInputStream());

      for (String s : result) {
        System.out.println(s);
        if (!s.contains("-classpath")) {

          String pid = s.substring(s.indexOf(" "), s.indexOf(" ", s.indexOf(" ") + 4)).trim();
          Runtime.getRuntime().exec("kill -9 " + pid);
        }
      }
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace(System.out);
    }

  }

  public static void main(String[] args)
  {
    new MockKiller().run();

  }

}
