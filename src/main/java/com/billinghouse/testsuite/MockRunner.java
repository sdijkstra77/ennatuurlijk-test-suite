package com.billinghouse.testsuite;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MockRunner extends Thread
{
  List<Process> processes = new ArrayList<Process>();

  @Override
  public void run()
  {
    File[] files = new File("src/test/mockservices").listFiles(new FilenameFilter()
    {
      @Override
      public boolean accept(File file, String fileName)
      {
        return fileName != null && fileName.toLowerCase().endsWith(".xml");
      }
    });

    for (File file : files) {
      String cmdLine = "/opt/SmartBear/SoapUI-5.2.1/bin/mockservicerunner.sh";
      String cmdArgs = "src/test/mockservices/" + file.getName();
      Process p = null;
      try {
        p = new ProcessBuilder(cmdLine, cmdArgs).start();
      }
      catch (IOException e) {
        e.printStackTrace(System.out);
      }
      if (p != null)
        processes.add(p);
    }
    boolean done = true;
    try {
      //    while (!done) {
      Thread.sleep(5000);
      //   }
    }
    catch (InterruptedException e) {
      e.printStackTrace(System.out);
    }
    finally {
      for (Process p : processes) {
        if (p.isAlive())
          p.destroy();
      }
    }

  }

  public static void main(String[] args) throws IOException
  {
    System.out.println("hi");

    new MockRunner().start();
  }

}
