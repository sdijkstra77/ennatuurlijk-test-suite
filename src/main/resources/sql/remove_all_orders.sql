delete from order_process;
delete from invoice_line;
delete from invoice_meta_field_map;
delete from invoice;

delete from order_line_meta_field_map where order_line_id in (select id from order_line);
delete from order_change;
delete from order_line;
delete from order_meta_field_map;
delete from mediation_order_map;
delete from invoice_line;
delete from invoice;
delete from order_process;
delete from purchase_order;
delete from settlement;

delete from customer_meta_field_map where customer_id in (select id from customer where user_id >= 130);
delete from event_log where user_id >= 130 or affected_user_id >= 130;
delete from customer_account_info_type_timeline where customer_id in (select id from customer where user_id >= 130);
delete from customer where user_id >= 130;
delete from process_run_user where user_id >= 130;
delete from user_role_map where user_id >= 130;
delete from partner where user_id >= 130;
delete from base_user where id >= 130;

delete from process_run_user;
delete from process_run_total;
delete from process_run;
delete from billing_process;
delete from mediation_process;
delete from event_log where message_id>1000;
