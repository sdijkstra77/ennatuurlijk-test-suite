package com.billinghouse.testsuite;

import com.eviware.soapui.support.SoapUIException;
import com.eviware.soapui.tools.SoapUITestCaseRunner;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.apache.xmlbeans.XmlException;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import java.io.IOException;
import java.net.ProxySelector;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SeleniumBilling
{

  private WebDriver driver;
  private static final String baseURL = "http://localhost:8080/jbilling-enn";

  @Test
  public void akillMocks()
  {
    new MockKiller().run();
  }

  @Test
  public void bstartMocks()
  {
    new MockRunner().run();
  }

  @Test
  public void cclearOrders() throws IOException
  {
    Runtime.getRuntime().exec("sudo -u postgres psql -d jbilling_enn -a -f /home/vagrant/queries/remove_all_orders.sql");
  }

  @Test
  public void daddOrder() throws Exception
  {
    ProxySelector proxy = ProxySelector.getDefault();
    SoapUITestCaseRunner runner = new SoapUITestCaseRunner();
    runner.setProjectFile("src/test/soapui/ennatuurlijk-soapui-project.xml");
    runner.run();
    ProxySelector.setDefault(proxy);
  }

  @Test
  public void erunJAR() throws XmlException, IOException, SoapUIException
  {
    //    DesiredCapabilities dcap = new DesiredCapabilities();
    //    File logfile = new File("./phantom.log");
    //    String[] phantomArgs = new String[] { "--webdriver-loglevel=DEBUG --ssl-protocol=any" };
    //    PhantomJSDriverService pjsds =
    //      new PhantomJSDriverService.Builder()
    //          .usingPhantomJSExecutable(new File("/opt/phantomjs-2.1.1-linux-x86_64/bin/phantomjs")).usingAnyFreePort()
    //          .usingCommandLineArguments(phantomArgs).withLogFile(logfile).build();
    //    driver = new PhantomJSDriver(pjsds, dcap);
    //    driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    driver = new HtmlUnitDriver()
    {
      @Override
      protected WebClient newWebClient(BrowserVersion version)
      {
        WebClient webClient = super.newWebClient(version);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        return webClient;
      }
    };
    ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
    try {
      login();
      runForecast("01-03-2014");
      runForecast("01-04-2014");
      runForecast("01-05-2014");
      setPreference("1016", "01-06-2014");
      runMediation();
      runJarEar("01-06-2014");
      runForecast("01-07-2014");
      runForecast("01-08-2014");
      runForecast("01-09-2014");
      runForecast("01-10-2014");
    }
    catch (Exception e) {
      e.printStackTrace(System.out);
    }
    driver.close();
    driver.quit();
  }

  @Test
  public void fendOrder() throws Exception
  {
    ProxySelector proxy = ProxySelector.getDefault();
    SoapUITestCaseRunner runner = new SoapUITestCaseRunner();
    runner.setProjectFile("src/test/soapui/ennatuurlijk-soapui-project.xml");
    runner.run();
    ProxySelector.setDefault(proxy);

  }

  // @Test
  public void grunEAR()
  {

    driver = new HtmlUnitDriver();
    try {
      login();
      runJarEar("01-11-2014");
    }
    catch (Exception e) {
      e.printStackTrace(System.out);
    }
    driver.close();
    driver.quit();
  }

  private void setPreference(String id, String value) throws InterruptedException
  {
    driver.get(baseURL + "/config/index/");
    Thread.sleep(2000);
    driver.findElement(By.id(id)).click();
    Thread.sleep(2000);
    driver.findElement(By.name("preference.value")).clear();
    driver.findElement(By.name("preference.value")).sendKeys(value);
    Thread.sleep(1000);
    driver.findElement(By.cssSelector(".submit.save")).click();
    Thread.sleep(1000);

  }

  private void login() throws InterruptedException
  {
    driver.get(baseURL + "/");
    String login = driver.getPageSource().substring(0, 200);
    driver.findElement(By.id("j_username")).sendKeys("admin");
    driver.findElement(By.id("j_password")).sendKeys("123qwe");
    WebElement element = driver.findElement(By.cssSelector(".submit.save"));
    element.click();
    try {
      driver.findElement(By.cssSelector(".submit.save")).click();

    }
    catch (Exception ex) {
      // trap error in login
    }
    Thread.sleep(2000);

    while (driver.getPageSource().substring(0, 200).equals(login)) {
      // The page hasn't changed.
      Thread.sleep(500);
      System.out.println("waiting");
    }
    System.out.println(driver.getPageSource());
  }

  private void runMediation() throws InterruptedException
  {
    driver.get(baseURL + "/mediationConfig/list");
    Thread.sleep(1000);
    driver.get(baseURL + "/mediationConfig/run");
    Thread.sleep(3000);
    driver.get(baseURL + "/billingconfiguration/index/");
    Thread.sleep(2000);

  }

  private void runJarEar(String runDate) throws InterruptedException
  {
    driver.get(baseURL + "/billingconfiguration/index/");
    Thread.sleep(1000);
    driver.findElement(By.id("nextRunDate")).clear();
    driver.findElement(By.id("nextRunDate")).sendKeys(runDate);
    if (driver.findElement(By.id("onlyRecurring")).isSelected())
      driver.findElement(By.id("onlyRecurring")).click();
    Thread.sleep(1000);
    driver.findElement(By.cssSelector(".submit.save")).click();
    Thread.sleep(2000);
    driver.get(baseURL + "/billingconfiguration/runBilling/");
    Thread.sleep(1000);
  }

  private void runForecast(String runDate) throws InterruptedException
  {
    driver.get(baseURL + "/billingconfiguration/index/");
    Thread.sleep(1000);
    driver.findElement(By.id("nextRunDate")).clear();
    driver.findElement(By.id("nextRunDate")).sendKeys(runDate);
    if (!driver.findElement(By.id("onlyRecurring")).isSelected())
      driver.findElement(By.id("onlyRecurring")).click();
    Thread.sleep(1000);
    driver.findElement(By.cssSelector(".submit.save")).click();
    Thread.sleep(2000);
    driver.get(baseURL + "/billingconfiguration/runBilling/");
    Thread.sleep(1000);

  }

}
